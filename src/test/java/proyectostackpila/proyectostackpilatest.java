package proyectostackpila;

import static org.junit.Assert.*;

import java.awt.List;
import java.util.ArrayList;

import static org.hamcrest.CoreMatchers.is;

import org.junit.Test;

import cl.ubb.proyectostackpila.proyectostackpila;

public class proyectostackpilatest {

	private static final int IsEmpty = 0;
	
	@Test
	public void PrimerStackVacioRetornaVerdadero() {
		/*arrange*/
		proyectostackpila stack = new proyectostackpila();
	    boolean resultado;      
	    
	    /*act*/
	    resultado = stack.DeterminarStack(IsEmpty);
	    
	    /*assert*/
	    assertThat(resultado,is(true));
	}
	
	
	@Test
	public void AgregarNumeroUnoStackRetornaFalsoNoVacio() {
		/*arrange*/
		proyectostackpila stack = new proyectostackpila();
	    boolean resultado;      
	    
	    /*act*/
	    resultado = stack.DeterminarStack(1);
	    
	    /*assert*/
	    assertThat(resultado,is(false));
	}
	
	
	@Test
	public void AgregarNumeroUnoyDosStackRetornaFalsoNoVacio() {
	    
		/*arrange*/
		proyectostackpila stack = new proyectostackpila();
	    boolean resultado;      
	    
	    /*act*/
	    resultado = stack.DeterminarStack(1);
	    resultado = stack.DeterminarStack(2);
	    
	    /*assert*/
	    assertThat(resultado,is(false));
	}
}
